Source: ocaml-sqlite3
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Stéphane Glondu <glondu@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 pkg-config,
 ocaml-nox (>= 4.01),
 libsqlite3-dev (>= 3.3.9),
 ocaml-dune,
 libdune-ocaml-dev,
 ocaml-findlib (>= 1.4),
 dh-ocaml
Standards-Version: 4.5.0
Rules-Requires-Root: no
Section: ocaml
Homepage: http://mmottl.github.io/sqlite3-ocaml
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml-sqlite3.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml-sqlite3

Package: libsqlite3-ocaml
Architecture: any
Depends:
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Description: Embeddable SQL Database for OCaml Programs (runtime)
 This package provides an interface for the SQLite3 embeddable SQL
 database. It allows your OCaml programs to use a SQL database without
 depending on any external software; the resulting system can be
 entirely self-contained.
 .
 This package provides the files necessary to run dynamically-linked
 OCaml programs with SQLite3.

Package: libsqlite3-ocaml-dev
Architecture: any
Depends:
 libsqlite3-dev,
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: Embeddable SQL Database for OCaml Programs (development)
 This package provides an interface for the SQLite3 embeddable SQL
 database. It allows your OCaml programs to use a SQL database without
 depending on any external software; the resulting system can be
 entirely self-contained.
 .
 This package provides the files necessary to develop new applications that
 use SQLite3 for OCaml.
